//
//  lsof.cpp
//  blc_channels
//
//  Created by Arnaud Blanchard on 18/04/2016.
//
//

#include "blc_process.h"
//#include "blc_program.h"
//#include "blc_command.h"
#include "blc_core.h"
#include <unistd.h> //read
#include <errno.h> //EINTR
#include <sys/wait.h> //waitpid
#include <inttypes.h>

extern char** environ; //From unistd for env var

blc_file *blc_files=NULL;
int files_nb=0;

blc_process *blc_processes=NULL;
int blc_processes_nb=0;

blc_file_opening::file_opening():file(NULL), process(NULL){};

blc_file::blc_file(): openings_pt(NULL),openings_nb(0){};
blc_process::blc_process():children(NULL), children_nb(0), openings_pt(NULL), openings_nb(0){
    
}

void blc_process::update_children(){
    blc_process tmp_process, *child;
    char *command;
    FILE *pgrep_file;
    
    destroy_children();

    SYSTEM_ERROR_CHECK(asprintf(&command, "pgrep -P %d", pid), -1, NULL);
    SYSTEM_ERROR_CHECK(pgrep_file=popen(command, "r"), NULL, "Command is: '%s'", command);
    FREE(command);
    while(fscanf(pgrep_file, "%d\n", &tmp_process.pid)==1){
        APPEND_ITEM(&children, &children_nb, &tmp_process);
    }
    pclose(pgrep_file);
    if (children) blc_processes_refresh(children, children_nb);
    FOR_EACH(child, children, children_nb)  child->update_children();

}

blc_process::~blc_process(){
    destroy_children();
}

void blc_process::destroy_children(){
    blc_process *child;
    FOR_EACH(child, children, children_nb){
        child->~blc_process();
    }
    FREE(children);
    children_nb=0;
}

void destroy_file_infos(blc_file **files_infos, int *files_infos_nb)
{
    blc_file *current_file;
    blc_file_opening **opening_pt;
    
    FOR_EACH(current_file, *files_infos, *files_infos_nb) {
        //Be carefule blc_process may used these openings
        FOR_EACH(opening_pt, current_file->openings_pt, current_file->openings_nb){
            FREE(*opening_pt);
        }
        FREE(current_file->openings_pt);
        current_file->openings_nb=0;
    }
    FREE(*files_infos);
    *files_infos_nb=0;
}

blc_process *blc_find_process(pid_t pid)
{
    int i;
    
    FOR_INV(i, blc_processes_nb) if (blc_processes[i].pid==pid) return &blc_processes[i];
    return NULL;
}

/*
 blc_file *blc_find_file(char const *name)
 {
 int i;
 FOR_INV(i, files_nb) if (strcmp(files[i].channel->name, name)==0) return &files[i];
 return NULL;
 }*/


void blc_processes_refresh_files(blc_process *processes, int processes_nb)
{
    blc_mem mem;
    char buf[LINE_MAX];
    char *path, *str;
    int stdout_pipe[2];
    pid_t pid;
    char letter;
    ssize_t n;
    int status, pos;
    blc_process *process=NULL, tmp_process;
    blc_file_opening *file_opening=NULL;
    blc_file *file, **file_pt, **files_pt=NULL;
    int files_nb=0;
    char pid_str[NAME_MAX];
    int pid_str_len, i;
    
    SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
    pid=fork();
    if (pid==0)
    {
        //SPRINTF(buf, "-p^%d", blaar_pid); Exclude itself
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
        blc_close_pipe(stdout_pipe);
        FOR(i, processes_nb) {
            pid_str_len=sprintf(pid_str, "%d,", processes[i].pid);
            mem.append(pid_str, pid_str_len);
        }
        mem.append("", 1); //last terminal nullchar
        //-a means AND selection instead of default OR, -d0-999 means only physical file (no socket etc )
        SYSTEM_ERROR_CHECK(execlp("lsof", "lsof", "-a", "-p", mem.chars,  "-wlnP", "-F", "aditn", "-d", "0-999", NULL), -1, NULL);
        mem.allocate(0);
    }
    else
    {
        mem.chars=NULL;
        close(stdout_pipe[1]);
        //     destroy_file_infos(&files, &files_nb);
        do{
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stdout_pipe[0], buf, sizeof(buf)), -1, EINTR, "read lsof stdout");
            mem.append(buf, n);
        }while(n!=0);
        waitpid(pid, &status, 0);
        close(stdout_pipe[0]);
        
        mem.append("", 1); //end of string
        
        str=mem.chars;
        do{
            letter=str[0];
            pos=0;
            
            str++;
            switch (letter){
                case 'a':
                    file_opening->access=str[0];
                    break;
                case 'd':
                    sscanf(str, "%s\n%n", file->device, &pos);
                    break;
                case 'f':
                    file_opening=new(blc_file_opening);
                    file_opening->process=process;
                    SYSTEM_ERROR_CHECK(sscanf(str, "%s\n%n", file_opening->fd, &pos), -1,"Scanning '%s'", str);
                    APPEND_ITEM(&process->openings_pt, &process->openings_nb, &file_opening);
                    file=new(blc_file);
                    break;
                case 'i':
                    SYSTEM_SUCCESS_CHECK(sscanf(str, "%llu\n%n", &file->inode, &pos), 1, "Scanning '%s'", str);
                    break;
                case 'n':
                    path=str;
                    str=strchr(str, '\n');
                    *str=0; //Replace last \n by terminate null char
                    str++;
                    FOR_EACH(file_pt, files_pt, files_nb)  {
                        if ((strcmp(file->type, "PIPE")==0) && strcmp(file_pt[0]->type, "PIPE")){
                            if (strcmp( file_pt[0]->device, path+2)==0) break; //path+2 to remove '->'
                        }else if (strcmp(file_pt[0]->path, path)==0) break;
                    }
                    
                    if (file_pt==files_pt+files_nb) { //FIle not found
                        STRCPY(file->path, path);
                        APPEND_ITEM(&files_pt, &files_nb, &file);
                    }
                    else if (strcmp(file->type, "PIPE")==0){
                        APPEND_ITEM(&file_pt[0]->openings_pt, &file_pt[0]->openings_nb, &file_opening);
                    }
                    else {
                        if (strcmp(file_pt[0]->type, file->type)!=0) EXIT_ON_ERROR("The recorded file type '%s' does not fit with the new file type '%s'",  file_pt[0]->type, file->type);
                        APPEND_ITEM(&file_pt[0]->openings_pt, &file_pt[0]->openings_nb, &file_opening);
                        delete(file);
                        file=*file_pt;
                    }
                    file_opening->file=file;

                    break;
                case 'p':
                    SYSTEM_SUCCESS_CHECK(sscanf(str, "%d\n%n", &pid, &pos), 1, NULL);
                    FOR_EACH(process, processes, processes_nb) if (process->pid==pid) break;
                    if (process==processes+processes_nb) EXIT_ON_ERROR("Process '%d' not found in lsof", process->pid);
                    break;
                case 't':
                    SYSTEM_ERROR_CHECK(sscanf(str, "%s\n%n", file->type, &pos), -1, "Scanning '%s' for type", str);
                    break;
                default:
                    str=strchr(str, '\n')+1; //We skip uninteresting info
                    break;
            }
            str+=pos;
        }while(str[0]!=0);
        mem.allocate(0);
    }
}

void blc_processes_refresh(blc_process *processes, int processes_nb){
    blc_mem mem;
    char buf[4096];
    int stdout_pipe[2], n, status, length, pos;
    pid_t pid;
    char **argv=NULL;
    int argc=0;
    int ret;
    char  *arg;
    char  *line;
    blc_process *process, tmp_process;
    char tmp_arg[NAME_MAX];
    
    SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
    
    pid=fork();
    if (pid==0){

        SYSTEM_ERROR_CHECK(setenv("LANG", "C", 1), -1, NULL);
        blc_add_arg(&argc, &argv, "ps", NULL);
        blc_add_arg(&argc, &argv, "-o", "ppid");
        blc_add_arg(&argc, &argv, "-o", "pid");
        blc_add_arg(&argc, &argv, "-o", "%cpu");
        blc_add_arg(&argc, &argv, "-o", "rss");
        blc_add_arg(&argc, &argv, "-o", "%mem");
        blc_add_arg(&argc, &argv, "-o", "comm");
        blc_add_arg(&argc, &argv, "-o", "command");
        
        if (processes==NULL) blc_add_arg(&argc, &argv, "-A", NULL);
        else{
            FOR_EACH_INV(process, processes, processes_nb){
                SPRINTF(tmp_arg, "%d", process->pid);
                arg=strdup(tmp_arg); //Do not need to be freed, it will be freed when execvp
                blc_add_arg(&argc, &argv, "-p", arg);
            }
        }
        blc_add_arg(&argc, &argv, NULL, NULL);//The memory will never be freed but it is small. We keep it during all the program (i.e. until execvp).
        
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
        blc_close_pipe(stdout_pipe);
        
        SYSTEM_ERROR_CHECK(execvp("ps", argv), -1, NULL);
    }
    else
    {
        close(stdout_pipe[1]);
        do
        {
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stdout_pipe[0], buf, sizeof(buf)), -1, EINTR, "read ps stdout");
            mem.append(buf, n);
        }while(n!=0);
        close(stdout_pipe[0]);
        
        mem.append("", 1); //end of string
        waitpid(pid, &status, 0);
        
        sscanf(mem.chars, "%*[^\n]\n%n", &pos); // remove title
        if (mem.size>1)
        {
            line=mem.chars+pos;
          //  fprintf(stderr, "%s\n", line);
            while((ret=sscanf(line, " %d %d %f %d %f %s %"STRINGIFY_CONTENT(LINE_MAX)"[^\n]\n%n", &tmp_process.parent_pid, &tmp_process.pid,   &tmp_process.cpu_percent, &tmp_process.mem, &tmp_process.mem_percent, tmp_process.command, tmp_process.full_command_line, &length))==7){
                FOR_EACH_INV(process, processes, processes_nb) if (process->pid == tmp_process.pid) *process=tmp_process; //not very efficient
                line+=length;
            }
            if (ret!=-1) EXIT_ON_ERROR("Parsing '%d' args instead of 7 in '%s'", ret, line);
        }
        mem.allocate(0);
    }
    blc_processes_refresh_files(processes, processes_nb);
}

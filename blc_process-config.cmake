find_package(blc_core REQUIRED)

find_path(BLC_PROCESS_INCLUDE_DIR blc_process.h PATH_SUFFIXES blc_process)
find_library(BLC_PROCESS_LIBRARY blc_process )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blc_process DEFAULT_MSG  BLC_PROCESS_LIBRARY BLC_PROCESS_INCLUDE_DIR)

mark_as_advanced(BLC_PROCESS_INCLUDE_DIR BLC_PROCESS_LIBRARY )

set(BL_INCLUDE_DIRS ${BLC_PROCESS_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BLC_PROCESS_LIBRARY} ${BL_LIBRARIES} )

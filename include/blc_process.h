//
//  lsof.hpp
//  blc_channels
//
//  Created by Arnaud Blanchard on 18/04/2016.
//
//

#ifndef BLC_PROCESS_H
#define BLC_PROCESS_H

#include <unistd.h> //pid_t
#include <limits.h> //NAME_MAX LINE_MAX
#include <stdint.h> //uint64_t



typedef struct blc_file blc_file;

typedef struct file_opening
{
#ifdef __cplusplus
    file_opening();
#endif
    blc_file *file;
    struct blc_process *process;
    char access;
    char fd[32];
}blc_file_opening;

typedef struct blc_file
{
#ifdef __cplusplus
    blc_file();
#endif
    
    uint64_t inode;
    char device[32];
    char path[PATH_MAX];
    char type[8];
    blc_file_opening **openings_pt;
    int openings_nb;
}blc_file;

typedef struct blc_process
{
#ifdef __cplusplus
    blc_process();
    ~blc_process();

    void destroy_children();
    void update_children();
#endif
    char command[NAME_MAX+1];
    char full_command_line[LINE_MAX];
    float cpu_percent, mem_percent;
    int mem;
    pid_t pid, parent_pid;
    struct blc_process *children;
    int children_nb;
    struct file_opening **openings_pt;
    int openings_nb;
}blc_process;

extern blc_process *blc_processes;
extern int blc_processes_nb;

blc_file *blc_find_file(char const *name);
blc_process *blc_find_process(pid_t pid);

//void blc_update_shared_files(blc_process **processes, int *processes_nb, blc_channel *channels, int channels_nb);

//void blc_update_shared_files(blc_channel *channels, int channels_nb);
void blc_processes_refresh(blc_process *processes, int processes_nb);
void blc_processes_refresh_files(blc_process *processes, int processes_nb);



#endif /* lsof_hpp */
